from django import forms

class Universeform(forms.Form):
    index=forms.IntegerField()
    name=forms.CharField()
    description=forms.Textarea()
    age=forms.IntegerField()
    satellites=forms.CharField()
    radius=forms.IntegerField()
    volume=forms.IntegerField()
    mass=forms.IntegerField()
    density=forms.IntegerField()
    distance_from_sun=forms.IntegerField()
    distance_from_earth=forms.IntegerField()
    day_length=forms.TimeField()
    