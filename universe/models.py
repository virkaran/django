from django.db import models

class Universe(models.Model):
    
    index=models.IntegerField()
    name=models.CharField('Name',max_length=100)
    description=models.TextField()
    age=models.IntegerField('Age')
    satellites=models.CharField('satellites',max_length=100)
    radius=models.IntegerField()
    volume=models.IntegerField()
    mass=models.IntegerField()
    density=models.IntegerField()
    distance_from_sun=models.IntegerField()
    distance_from_earth=models.IntegerField()
    day_length=models.TimeField()

