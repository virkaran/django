from django.urls import path

from . import views

urlpatterns = [
    path('', views.add_planet, name='add_planet'),
    path('list_view',views.list_view,name='view_list')
]