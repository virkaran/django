from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from .forms import Universeform
from .models import Universe

def add_planet(request):
    context={}
    form=Universeform(request.POST)
    if form.is_valid():
        form.save()
    
    context['form']= form 
    # if request.method == 'POST':
    #     form = Universeform(request.POST)
    #     if form.is_valid():
    #         form.save()
    # else:
    #     form = Universeform()

    return render(request, 'add.html', {'form': form})


def list_view(request): 

	context ={} 
	context['dataset']=Universe.objects.all()
	return render(request, "details.html", context) 


# def view_details(request):
#     details=Universe.objects.all()
#     return render(request,'details.html',{'details':details})

# def delete_planet(request,id):
#     details=Universe.objects.get(id=id)
#     details.delete()
#     return redirect('/')

# def update_view(request):
#     details=Universeform.objects.get(id=id)
#     form=Universeform(request.POST,instance='details')
#     if form.is_valid():
#         form.save(commit=True)
     

